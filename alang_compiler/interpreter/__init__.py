from typing import Callable

from alang_compiler.ir import *


class Call:
    def __init__(self, return_line: int, return_variable: IRVariable):
        self.return_line = return_line
        self.return_variable = return_variable
        self.allocated_in_call = []

    def __str__(self):
        return f"{self.__class__.__name__}({self.return_line}, {self.return_variable.get_name()})"


class AllocatedVariable:
    def __init__(self, var_name: str, address: int, size: int):
        self.var_name = var_name
        self.address = address
        self.size = size

    def __str__(self):
        return f"{self.__class__.__name__}({self.var_name},{self.address},{self.size})"

    def __repr__(self):
        return self.__str__()


CALL_STACK_LIMIT = 64
MEMORY_SIZE = 256


class EvaluateIR(IRInterpreter):
    def __init__(self, intermediate: Intermediate):
        super(EvaluateIR, self).__init__(intermediate, True)
        self.builtins: dict[str, Callable] = {}
        self.variables: list[AllocatedVariable] = []
        self.memory: bytearray = bytearray(b'\0' * MEMORY_SIZE)
        self.occupied: dict[int, bool] = {}
        for i in range(MEMORY_SIZE):
            self.memory[i] = 0
            self.occupied[i] = False

        self.args: list[IRVariable] = []
        self.call_stack: list[Call] = []
        # self.labels: list[str] = []

    def _get_value(self, ir_var: IRVariable) -> int:
        if isinstance(ir_var, IRConst):
            return ir_var.get_name()

        alloc: AllocatedVariable = self._get_variable_allocation(ir_var.get_name())
        return self._get_memory(alloc.address, alloc.size)

    def _get_memory(self, address: int, size: int) -> int:
        return int.from_bytes(self.memory[address:address + size], byteorder='little', signed=False)

    def _get_variable_allocation(self, name: str) -> AllocatedVariable:
        for var in reversed(self.variables):
            if var.var_name == name:
                debug(None, "Got var:", str(var), self._get_memory(var.address, var.size), importance=6)
                return var
        raise ValueError('No variable found with name: %s' % name)

    def _free_memory(self, alloc: AllocatedVariable):
        for i in range(alloc.address, alloc.address+alloc.size):
            self.occupied[i] = False

        self.variables.remove(alloc)

    def _allocate_memory(self, name: str, size: int) -> AllocatedVariable:
        consecutive_space = 0
        begin = 0
        while consecutive_space < size and begin + consecutive_space < MEMORY_SIZE:
            if self.occupied[begin]:
                begin += 1
                consecutive_space = 0
            else:
                # print(begin, consecutive_space)
                if consecutive_space == 0:
                    consecutive_space += 1
                space_ok = not any(
                    self.occupied[i]
                    for i in range(begin, begin + consecutive_space)
                )

                if space_ok:
                    consecutive_space += 1
        if consecutive_space >= size:
            for i in range(begin, begin + consecutive_space):
                self.occupied[i] = True

            return AllocatedVariable(name, begin, size)
        else:
            raise MemoryError(f'No enough consecutive space available for {name}, ' + \
                              f'Only {consecutive_space} bytes left')

    def _set_value(self, var: Union[IRVariable, str], value: Union[int, bytearray, bytes]):
        name = var
        if type(var) == IRVariable:
            name = var.get_name()
        alloc: AllocatedVariable = self._get_variable_allocation(name)
        if type(value) == int:
            value = bytearray([value >> (i * 8) & 0xff for i in range(alloc.size)])
        self.memory[alloc.address:alloc.address + alloc.size] = value

    def _prefixed_name(self, name: str) -> str:
        return f"{name}_{len(self.call_stack)}"

    def _jump_to_address(self, address: int):
        debug(None, f"Jumped to address {address - 1}", importance=6)
        self.cursor = address - 1

    def QAllocate(self, ir: QAllocate):
        alloc: AllocatedVariable = self._allocate_memory(ir.ir_var.get_name(), ir.size)
        debug(None, f"Allocated new variable: {alloc}", importance=6)
        self.variables += [alloc]
        if len(self.call_stack):
            self.call_stack[-1].allocated_in_call += [alloc]
        if len(self.args):
            value = self._get_value(self.args.pop())
            self._set_value(alloc.var_name, value)

    def QCall(self, ir: QCall):
        result_name = ir.result.get_name()
        func_name = ir.func_name
        if type(func_name) == str:
            self.builtins[func_name](result_name, self.args)
        else:
            if len(self.call_stack) > CALL_STACK_LIMIT:
                debug(None, self._get_memory_repr())
                debug(None, '\n'.join(str(c) for c in self.call_stack))
                raise RecursionError(
                    f'Too many calls, maybe an unbound recursion? Call stack limit: {CALL_STACK_LIMIT}')
            self.call_stack.append(Call(self.cursor + 1, ir.result))

            self._jump_to_address(func_name.line - 1)

    def QLabel(self, ir: QLabel):
        pass

    def QAssign(self, ir: QAssign):
        value = self._get_value(ir.value)
        debug(None, "Value in assign:", value)

        self._set_value(ir.result, value)

    def QBinaryOperator(self, ir: QBinaryOperator):
        operand_1 = self._get_value(ir.operand_1)
        operand_2 = self._get_value(ir.operand_2)
        operator = ir.operator
        value = operator(operand_1, operand_2)
        self._set_value(ir.result, value)

    def QReturn(self, ir: QReturn):
        return_call = self.call_stack.pop()
        value = self._get_value(ir.ir_var)
        self._set_value(return_call.return_variable, value)
        debug(None, "Memory to be freed:", return_call.allocated_in_call, importance=6)
        for var in return_call.allocated_in_call:
            self._free_memory(var)

        self._jump_to_address(return_call.return_line)

    def IRComment(self, ir: IRComment):
        pass

    def QHalt(self, ir: QHalt):
        self.halted = True

    def QCondition(self, ir: QCondition):
        condition = self._get_value(ir.ir_var)
        if not condition:
            self._jump_to_address(ir.false_label.line - 1)

    def QIncrement(self, ir: QIncrement):
        res = self._get_value(ir.result)
        res += 1
        self._set_value(ir.result, res)

    def QJump(self, ir: QJump):
        self._jump_to_address(ir.label.line - 1)

    def QParam(self, ir: QParam):
        self.args.append(ir.ir_var)

    def _get_memory_repr(self) -> str:
        s = "== MEMORY ==\n"
        indentation = max(len(hex(key)) for key in self.memory)
        for i in range(0, len(self.memory), 2 * 4):
            address = i * 2 * 4
            value = self.memory[i * 2 * 4:(i + 1) * 2 * 4]
            if value != bytearray(b''):
                address_str = hex(address)
                i_key = address_str + ":" + " " * (indentation - len(address_str))
                s += f"{i_key}"
                for octet in value:
                    s += f" {hex(octet)[2:].zfill(2)}"
                s += "\n"

        for var in self.variables:
            res = ' '.join(hex(x)[2:].zfill(2) for x in self.memory[var.address:var.address + var.size])
            header = f"{var.var_name} at {hex(var.address)} - {hex(var.address + var.size)}"
            s += f"{header:<32} = {res}\n"

        s += "OCCUPATION:\n"
        for i, occupied in self.occupied.items():
            if occupied:
                s += f"{hex(i)[2:].zfill(2)}: {occupied}, "
        s += "\n"
        return s

    def evaluate(self):
        try:
            # TODO: Allocate argc and argv
            super().evaluate()

        except Exception as e:
            error(None, e, e.__dict__)
            raise e
        finally:
            debug(None, f"cursor={self.cursor}")
            debug(None, self._get_memory_repr())

            debug(None, "Leftover args:")
            for val in self.args:
                debug(None, f"{val}: {val.get_name()}")
