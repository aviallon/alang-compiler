from typing import Dict, Union, Callable, Any, Optional

import lark
import logging
import sys


class Debug:
    DEBUG_LEVEL = 4
    message_counts: Dict[str, int] = {}
    log_functions = {
        "debug": lambda x: print("DEBUG:", x, sep='', flush=True),
        "warning": lambda x: print("WARNING:", x, sep='', file=sys.stderr, flush=True),
        "error": lambda x: print("ERROR:", x, sep='', file=sys.stderr, flush=True),
        "critical": lambda x: print("CRITICAL:", x, sep='', file=sys.stderr, flush=True),
        "info": lambda x: print("INFO:", x, sep='', flush=True)
    }
    input_lines: Optional[list[str]] = None


def check(assertion: bool, error_msg: str, tree: lark.Tree) -> bool:
    if not assertion:
        msg = error_msg
        if hasattr(tree, "data"):
            msg += f" (in {tree.data})"
        error(tree, msg)
    return assertion


def quote_tree(tree: lark.Tree) -> str:
    return f"From {tree.line}:{tree.column} to {tree.end_line}:{tree.end_column}:\n" + quote_text(tree.line,
                                                                                                  tree.column,
                                                                                                  tree.end_line,
                                                                                                  tree.end_column,
                                                                                                  Debug.input_lines)


def quote_text(line: int, column: int, end_line: int, end_column: int, text: list[str]) -> str:
    if line != end_line:
        end_column = len(text[line])

    length = end_column - column
    first_half = length // 2
    second_half = length - first_half
    header_msg = "Here: "
    msg = ""
    msg += " " * len(header_msg) + text[line - 1]
    msg += "\n" + " " * len(header_msg) + " " * (column - 1) + "╧" * first_half + "╪" + "╧" * (
            second_half - 1)
    msg += "\n" + header_msg + "─" * (column - 1 + first_half) + "┘"
    return msg


def log(tree: Optional[lark.Tree], args: tuple[Any, ...], level: str, color: Optional[str] = None,
        importance: Optional[int] = None):
    if level not in Debug.message_counts:
        Debug.message_counts[level] = 0
    Debug.message_counts[level] += 1

    if importance is not None and Debug.DEBUG_LEVEL < importance:
        return

    msg = " ".join(str(x) for x in args)
    position_available = hasattr(tree, "line") and hasattr(tree, "column")
    position_info = ""
    color_begin = "" if color is None else f"\033[{str(color)}m"
    color_end = "" if color is None else "\033[0m"
    if importance is not None and importance < 3:
        position_info += f"\033[31mL{str(importance)}" + color_begin + ":"
    if position_available:
        line = tree.line
        column = tree.column
        position_info += f"{line}:{column}:"

    msg = color_begin + position_info + " " + msg + color_end

    if position_available and level in ["warning", "error"]:
        msg += "\n" + quote_tree(tree)

    Debug.log_functions[level](msg)


def critical(tree: Optional[lark.Tree], *args):
    log(tree, args, 'critical', color="91")


def error(tree: Optional[lark.Tree], *args):
    log(tree, args, 'error', color="91")


def warn(tree: Optional[lark.Tree], *args, importance: int = 1):
    log(tree, args, 'warning', color="33", importance=importance)


def debug(tree: Optional[lark.Tree], *args, importance: int = 4):
    log(tree, args, 'debug', color="36", importance=importance)


def info(tree: Optional[lark.Tree], *args, importance: int = 3):
    log(tree, args, 'info', color="37", importance=importance)


class Type:
    def __init__(self, name: str, size: int = 4):
        self.name = name
        self.size = size

    def __str__(self):
        return self.name

    def __repr__(self):
        return self.__str__()


class Operator:
    def __init__(self, name: str, symbol: str, operation: Callable[[int, int], int]):
        self.name = name
        self.symbol = symbol
        self.operation = operation

    def __str__(self):
        return self.symbol

    def __call__(self, a: int, b: int):
        return self.operation(a,b)

    def __repr__(self):
        return self.__str__()


class TYPES:
    VOID = Type("void", 0)
    INT = Type("int", 4)
    BOOL = Type("bool", 1)
    UNDEFINED = Type("undefined", 0)


class OPERATORS:
    PLUS = Operator("PLUS", "+",
                    (lambda a, b: a + b))
    MINUS = Operator("MINUS", "-",
                     (lambda a, b: a - b))
    TIMES = Operator("TIMES", "*",
                     (lambda a, b: a * b))
    DIVIDE = Operator("DIVIDE", "/",
                      (lambda a, b: a // b))
    MODULO = Operator("MODULO", "%",
                      (lambda a, b: a % b))
    EQUALS = Operator("EQUALS", "==",
                      (lambda a, b: int(a == b)))
    DIFFERENT = Operator("DIFFERENT", "!=",
                         (lambda a, b: int(a != b)))
    AND = Operator("AND", "&&",
                   (lambda a, b: int(bool(a) and bool(b))))
    OR = Operator("OR", "||",
                  (lambda a, b: int(bool(a) or bool(b))))
    LESS = Operator("LESS", "<",
                    (lambda a, b: int(a < b)))
    LESS_EQUAL = Operator("LESS_EQUAL", "<=",
                          (lambda a, b: int(a <= b)))
    GREATER = Operator("GREATER", ">",
                       (lambda a, b: int(a > b)))
    GREATER_EQUAL = Operator("GREATER_EQUAL", ">=",
                             (lambda a, b: int(a >= b)))


def get_value(parent: lark.Tree, tree: Union[lark.Tree, int, bool]):
    if type(tree) in [int, bool]:
        return tree
    if hasattr(tree.meta, "value"):
        return tree.meta.value
    elif tree.data == "macro_line":
        return parent.meta.line
    else:
        raise ValueError(f'No value for parameter: {tree}')


def get_type(tree: Union[lark.Tree, int, bool]) -> Type:
    if type(tree) == int:
        return TYPES.INT
    elif type(tree) == bool:
        return TYPES.BOOL
    elif not hasattr(tree, "data"):
        error(tree, f"This is not a tree, can't check its type: {tree}")
        return TYPES.UNDEFINED

    if hasattr(tree, "meta") and hasattr(tree.meta, "type"):
        return tree.meta.type

    tree_type = TYPES.UNDEFINED
    type_is_defined = True
    if len(tree.children) > 0:
        for child in tree.children:
            type_is_defined = type_is_defined and \
                              check(get_type(child) != TYPES.UNDEFINED, "Child has not type!", child)

        if type_is_defined:
            tree_type = get_type(tree.children[0])

        if len(tree.children) > 1 and type_is_defined:
            for child in tree.children[1:]:
                check(get_type(child) == tree_type,
                      f"Type mismatch in expression: wanted {tree_type}, got {child.meta.type}", child)

    return tree_type


def get_metadata(metadata):
    filter_out = ["empty", "start_pos", "end_pos"]

    res = {}
    position = {"line": None, "column": None, "end_line": None, "end_column": None}
    for key in metadata.__dict__:
        if key in position:
            position[key] = metadata.__dict__[key]
        elif key not in filter_out:
            res[key] = metadata.__dict__[key]

    position = {}
    return res, position
