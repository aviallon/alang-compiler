import lark
from beartype import beartype
from typing import Union, Optional
from alang_compiler.scope import Scope

from alang_compiler.utils import TYPES, get_metadata, get_type, OPERATORS, get_value, Operator, error, debug, \
    quote_text, Debug, check, info, warn, quote_tree


# noinspection PyMethodMayBeStatic
class TreeReducer(lark.Transformer):
    def __init__(self, comments=False):
        super().__init__()
        self.comments = comments

    @beartype
    def __default_token__(self, token: lark.Token):
        # print("Got a token: ", token.value, token.type)
        return token.value

    def IDENTIFIER(self, token: lark.Token):
        return str(token.value)

    def SIGNED_INT(self, token: lark.Token):
        return int(token.value)

    def CPP_COMMENT(self, token: lark.Token):
        return str(token.value)

    def MACRO_FUNCTION(self, token: lark.Token):
        return str(token.value)

    @beartype
    def expression(self, tree: list[lark.Tree]):
        return tree[0]

    @beartype
    def statement(self, tree: list[lark.Tree]):
        return tree[0]

    def true(self, tree: lark.Tree):
        return True

    def false(self, tree: lark.Tree):
        return False

    def bool_literal(self, tree: lark.Tree):
        return tree[0]

    def LESS_GREATER_OPERATORS(self, token: lark.Token):
        debug(None, "LESS_GREATER_OPERATORS", token.value, importance=6)
        value = token.value
        if value == "<":
            return OPERATORS.LESS
        elif value == "<=":
            return OPERATORS.LESS_EQUAL
        elif value == ">":
            return OPERATORS.GREATER
        elif value == ">=":
            return OPERATORS.GREATER_EQUAL

    def EQ_NEQ_OPERATORS(self, token: lark.Token):
        debug(None, "EQ_NEQ_OPERATORS", token.value, importance=6)
        value = token.value
        if value == "==":
            return OPERATORS.EQUALS
        elif value == "!=":
            return OPERATORS.DIFFERENT

    def PLUS_MINUS_OPERATORS(self, token: lark.Token):
        debug(None, "PLUS_MINUS_OPERATORS", token.value, importance=6)
        value = token.value
        if value == "+":
            return OPERATORS.PLUS
        elif value == "-":
            return OPERATORS.MINUS

    def TIMES_DIV_MOD_OPERATORS(self, token: lark.Token):
        debug(None, "TIMES_DIV_MOD_OPERATORS", token.value, importance=6)
        value = token.value
        if value == "*":
            return OPERATORS.TIMES
        elif value == "/":
            return OPERATORS.DIVIDE
        elif value == "%":
            return OPERATORS.MODULO


class AmbiguityRemover(lark.visitors.Transformer_InPlace):
    @beartype
    def _ambig(self, tree: list[lark.Tree]):
        return tree[0]


# noinspection PyMethodMayBeStatic
class AnnotateTree(lark.visitors.Interpreter):
    def __init__(self, root_scope: Optional[Scope] = None):
        if root_scope is None:
            root_scope = Scope('root')

        self.root_scope: Scope = root_scope
        self.print_filter = []
        self.current_scope: Scope = self.root_scope
        self.current_function: Optional[lark.Tree] = None
        self.current_loop: Optional[lark.Tree] = None

    @beartype
    def _new_scope(self, name: Optional[str] = None, is_namespace: bool = False) -> Scope:
        return self.current_scope.add_scope(Scope(name, self.current_scope, is_namespace=is_namespace))

    def _set_scope_to_parent(self):
        # Do some checks
        for key, variable in self.current_scope.variables.items():
            if not len(variable.meta.usages):
                warn(variable, "Variable is never used")

        self.current_scope = self.current_scope.parent

    @beartype
    def _get_variable(self, tree: lark.Tree, var_name: str, assigning: bool = False) -> Optional[lark.Tree]:
        variable_declaration = self.current_scope.lookup_variable(tree.meta.name)
        if variable_declaration is not None:
            debug(variable_declaration, variable_declaration.data, get_metadata(variable_declaration.meta))
            if not assigning and not variable_declaration.meta.assigned:
                msg = "Using potentially uninitialized variable!\n"
                msg += "Declared here:\n" + quote_tree(variable_declaration)
                warn(tree, msg)
            elif assigning:
                variable_declaration.meta.assigned = True

            tree.meta.type = variable_declaration.meta.type
            variable_declaration.meta.usages += [tree]
        else:
            tree.meta.type = TYPES.UNDEFINED
            error(tree, f"Variable {var_name} is not defined!")

        return variable_declaration

    @beartype
    def __default__(self, tree: lark.Tree):
        debug(tree, f"{tree.data} has no default visitor")
        super().visit_children(tree)

    @beartype
    def visit(self, tree: lark.Tree):
        super().visit(tree)
        if not (hasattr(tree.meta, "type")):
            tree.meta.type = TYPES.VOID

        if not (hasattr(tree.meta, "scope")):
            tree.meta.scope = self.current_scope.get_scope()

        if len(self.print_filter) == 0 or tree.data in self.print_filter:
            debug(tree, f"{tree.data=}, {get_metadata(tree.meta)=}", importance=6)
        return tree

    @beartype
    def axiom(self, tree: lark.Tree):
        self.visit_children(tree)

    @beartype
    def namespace_declaration(self, tree: lark.Tree):
        self.visit(tree.children[0])
        scope = self._new_scope(tree.children[0].meta.name)
        self.current_scope = scope
        for child in tree.children[1:]:
            self.visit(child)
        self._set_scope_to_parent()
        debug(tree, f"New namespace: {tree.children[0].meta.name}")

    @beartype
    def global_variables(self, tree: lark.Tree):
        self.visit_children(tree)

    @beartype
    def comment(self, tree: lark.Tree):
        self.visit_children(tree)

    @beartype
    def decorated_function(self, tree: lark.Tree):
        self.visit_children(tree)
        tree.meta.decorator = tree.children[0].meta.name
        func = tree.children[1]
        tree.meta.decorated_function = func
        tree.meta.arg_number = len(func.meta.formals)
        if tree.meta.decorator == "entrypoint":
            debug(tree, "Got the entrypoint!")
            self.root_scope.entrypoint = func
            if tree.meta.arg_number == 0:
                pass
            elif tree.meta.arg_number != 2:
                msg = f"Entrypoint must have either zero or two arguments, {func.meta.name} has {len(func.meta.formals)}\n"
                msg += quote_text(func.line, func.column, func.end_line, func.end_column, Debug.input_lines)
                error(tree.children[0], msg)
            else:
                if (arg_type := get_type(func.meta.formals[0])) != TYPES.INT:
                    msg = f"Entrypoint first parameter must be an integer, whereas it is {arg_type}\n"
                    msg += quote_text(func.line, func.column, func.end_line, func.end_column, Debug.input_lines)
                    error(tree.children[0], msg)
        tree.meta.name = tree.children[1].meta.name

    @beartype
    def function(self, tree: lark.Tree):
        self.visit(tree.children[0])
        tree.meta.type = get_type(tree.children[0])
        self.visit(tree.children[1])
        tree.meta.name = tree.children[1].children[0]

        self.current_scope.functions[tree.meta.name] = tree

        self.current_scope = self._new_scope(tree.meta.name)
        previous_function = self.current_function
        self.current_function = tree

        self.visit(tree.children[2])

        assert tree.children[2].data == "formals"
        tree.meta.formals = tree.children[2].children

        tree.meta.returns = []

        if len(tree.children) > 3:
            for child in tree.children[3:]:
                self.visit(child)
            tree.meta.statements = tree.children[3:]

        debug(tree, f"Function {tree.meta.name} return statements:", tree.meta.returns)
        if not tree.meta.returns and tree.meta.type != TYPES.VOID:
            error(tree, f"Function {tree.meta.name} is not void and expects a return statement")

        self.current_function = previous_function
        self._set_scope_to_parent()

    @beartype
    def function_call(self, tree: lark.Tree):
        self.visit_children(tree)
        tree.meta.name = tree.children[0].meta.name

        tree.meta.namespace = ['root'] + tree.children[0].meta.namespace
        tree.meta.arguments = tree.children[1].children
        lookup_scope = self.current_scope

        if len(tree.meta.namespace):
            lookup_scope = self.root_scope.lookup_scope(tree.meta.namespace)

        function_declaration = lookup_scope.lookup_function(tree.meta.name)
        if function_declaration is not None:
            tree.meta.type = function_declaration.meta.type

            debug(tree, "function_call:", function_declaration.meta.formals, tree.meta.arguments)
            if len(tree.meta.arguments) != len(function_declaration.meta.formals):
                msg = f'Wrong number of arguments for function {tree.meta.name}, '
                msg += f'expected {len(function_declaration.meta.formals)}, got {len(tree.meta.arguments)}\n'
                msg += f'Hint: {tree.meta.name} defined at {function_declaration.meta.line}:{function_declaration.meta.column}'
                error(tree, msg)

            else:
                arg_type_error = False
                msg = ""
                for index in range(len(tree.meta.arguments)):
                    arg_type = get_type(tree.meta.arguments[index])
                    if arg_type != get_type(function_declaration.meta.formals[index]):
                        if arg_type == TYPES.UNDEFINED:
                            continue
                        msg += f'Wrong argument type for function {tree.meta.name}, '
                        msg += f'expected {get_type(function_declaration.meta.formals[index])}, got {get_type(tree.meta.arguments[index])}\n'
                        arg_type_error = True

                if arg_type_error:
                    msg += f'Hint: {tree.meta.name} defined at {function_declaration.meta.line}:{function_declaration.meta.column}'
                    error(tree, msg)

        else:
            tree.meta.type = TYPES.UNDEFINED
            error(tree, f"function {tree.meta.name} is undefined!")

        # if self.current_scope['functions'][tree.meta.name]

    @beartype
    def arguments(self, tree: lark.Tree):
        self.visit_children(tree)

    @beartype
    def identifier(self, tree: lark.Tree):
        self.visit_children(tree)
        tree.meta.name = tree.children[0]

    @beartype
    def formals(self, tree: lark.Tree):
        self.visit_children(tree)

    @beartype
    def formal(self, tree: lark.Tree):
        self.visit_children(tree)
        tree.meta.type = get_type(tree.children[0])
        tree.meta.name = tree.children[1].meta.name
        tree.meta.assigned = True
        tree.meta.usages = []

        self.current_scope.variables[tree.meta.name] = tree

    @beartype
    def expr_assignment(self, tree: lark.Tree):
        self.visit_children(tree)
        tree.meta.name = tree.children[0].meta.name
        debug(tree, "expr_assignment", get_metadata(tree.children[1].meta))
        value_type = get_type(tree.children[1])
        tree.meta.type = value_type
        var_declaration = self._get_variable(tree.children[0], tree.meta.name, assigning=True)
        if var_declaration is not None:  # Do not repeat errors
            var_type = get_type(var_declaration)
            check(value_type == var_type,
                  f"assigning wrong type {value_type} to variable of type {var_type}", tree)

            debug(tree, "expr_assignment:", get_metadata(var_declaration))
        else:
            tree.meta.type = TYPES.UNDEFINED

    @beartype
    def scoped_identifier(self, tree: lark.Tree):
        self.visit_children(tree)
        if len(tree.children) > 1:
            tree.meta.namespace = [child.meta.name for child in tree.children[:-1]]
        else:
            tree.meta.namespace = []
        debug(tree, "scoped_identifier:", get_metadata(tree.meta))
        tree.meta.name = tree.children[-1].meta.name

    @beartype
    def variable_declaration(self, tree: lark.Tree):
        self.visit_children(tree)
        tree.meta.type = get_type(tree.children[0])
        tree.meta.name = tree.children[1].meta.name
        if tree.meta.name in self.current_scope.variables:
            msg = f"Redefinition of variable {tree.meta.name}\n"
            msg += "Previous declaration:\n"
            msg += quote_tree(self.current_scope.variables[tree.meta.name])
            error(tree, msg)

        tree.meta.expression = None
        tree.meta.assigned = False
        if len(tree.children) > 2:
            tree.meta.assigned = True
            tree.meta.expression = tree.children[2]
            expr_type = get_type(tree.meta.expression)
            if expr_type != tree.meta.type:
                msg = f"Trying to assign value of type {expr_type} to variable of type {tree.meta.type}\n"
                msg += quote_tree(tree.meta.condition)
                error(tree, msg)

        tree.meta.usages = []
        assert type(tree.meta.name) == str

        self.current_scope.variables[tree.meta.name] = tree

    @beartype
    def return_statement(self, tree: lark.Tree):
        self.visit_children(tree)
        debug(tree, "return statement", tree.children)
        if len(tree.children) == 1:
            tree.meta.type = get_type(tree.children[0])
            if tree.meta.type == TYPES.UNDEFINED:
                return
        else:
            tree.meta.type = TYPES.VOID
        if self.current_function is not None:
            func_meta = self.current_function.meta
            if func_meta.type != tree.meta.type:
                msg = f"Function is of type {func_meta.type}, while returned value is of type {tree.meta.type}"
                msg += "\nDeclaration:\n" + quote_text(func_meta.line, func_meta.column, func_meta.end_line,
                                                       func_meta.end_column, Debug.input_lines)
                error(tree, msg)
            self.current_function.meta.returns += [tree]
        else:
            error(tree, "Return statement not in a function")

    @beartype
    def expression(self, tree: lark.Tree):
        self.visit_children(tree)
        tree.meta.type = get_type(tree)

    @beartype
    def integer(self, tree: lark.Tree):
        tree.meta.type = TYPES.INT

    @beartype
    def boolean(self, tree: lark.Tree):
        tree.meta.type = TYPES.BOOL

    def macro_line(self, tree: lark.Tree):
        tree.meta.type = TYPES.INT
        tree.meta.value = tree.meta.line

    @beartype
    def literal(self, tree: lark.Tree):
        self.visit_children(tree)
        tree.meta.type = get_type(tree.children[0])
        tree.meta.value = get_value(tree, tree.children[0])
        debug(tree, "literal:", get_metadata(tree.meta), importance=6)

    @beartype
    def statement_block(self, tree: lark.Tree):
        self.current_scope = self._new_scope()
        self.visit_children(tree)
        self._set_scope_to_parent()

    @beartype
    def expr_increment(self, tree: lark.Tree):
        self.visit_children(tree)
        tree.meta.name = tree.children[0].meta.name
        var_declaration = self._get_variable(tree.children[0], tree.meta.name, assigning=True)
        if var_declaration is not None:  # Do not repeat errors
            tree.meta.type = get_type(var_declaration)
        else:
            tree.meta.type = TYPES.UNDEFINED

    @beartype
    def break_statement(self, tree: lark.Tree):
        self.visit_children(tree)
        if self.current_loop is not None:
            tree.meta.loop = self.current_loop
            self.current_loop.meta.breaks += [tree]
        else:
            error(tree, "break statement outside a loop")

    @beartype
    def while_loop(self, tree: lark.Tree):
        tree.meta.breaks = []

        parent_loop = self.current_loop
        self.current_loop = tree

        self.visit_children(tree)
        tree.meta.condition = tree.children[0]
        tree.meta.statement = tree.children[1]
        if get_type(tree.meta.condition) != TYPES.BOOL:
            error(tree.meta.condition, "Expected a boolean for condition expression")

        self.current_loop = parent_loop

    @beartype
    def for_loop(self, tree: lark.Tree):
        tree.meta.breaks = []

        self.visit_children(tree)
        tree.meta.initialization = tree.children[0]
        tree.meta.condition = tree.children[1]
        tree.meta.incrementation = tree.children[2]
        tree.meta.statement = tree.children[3]
        if get_type(tree.meta.condition) != TYPES.BOOL:
            error(tree.meta.condition, "Expected a boolean for condition expression")

    @beartype
    def if_statement(self, tree: lark.Tree):
        self.visit_children(tree)
        tree.meta.condition = tree.children[0]
        tree.meta.iftrue = tree.children[1]
        tree.meta.iffalse = None
        if len(tree.children) == 3:
            tree.meta.iffalse = tree.children[2]
        if get_type(tree.meta.condition) != TYPES.BOOL:
            error(tree.meta.condition, "Expected a boolean for condition expression")

    @beartype
    def statement(self, tree: lark.Tree):
        self.visit_children(tree)

    @beartype
    def expr_identifier(self, tree: lark.Tree):
        self.visit_children(tree)
        var_name = tree.children[0].meta.name
        tree.meta.name = var_name
        self._get_variable(tree, var_name)

    @beartype
    def _generic_expr_binary_op(self, tree: lark.Tree, operator: Optional[Operator] = None):
        self.visit_children(tree)
        if operator is None or len(tree.children) == 3:
            tree.meta.operator = tree.children[1]
            del tree.children[1]
        else:
            tree.meta.operator = operator

        tree.meta.type = get_type(tree)
        tree.meta.operand_1 = tree.children[0]
        tree.meta.operand_2 = tree.children[1]


    @beartype
    def expr_sum_sub(self, tree: lark.Tree):
        self._generic_expr_binary_op(tree)

    @beartype
    def expr_mul_div_mod(self, tree: lark.Tree):
        self._generic_expr_binary_op(tree)

    @beartype
    def expr_equals_nequals(self, tree: lark.Tree):
        self._generic_expr_binary_op(tree)
        tree.meta.type = TYPES.BOOL

    @beartype
    def expr_lesser_greater(self, tree: lark.Tree):
        self._generic_expr_binary_op(tree)
        tree.meta.type = TYPES.BOOL

    def _logical_common(self, tree: lark.Tree, operator: Operator):
        self._generic_expr_binary_op(tree, operator)
        if get_type(tree.meta.operand_1) != TYPES.BOOL:
            error(tree.meta.operand_1, "boolean type expected")
        if get_type(tree.meta.operand_2) != TYPES.BOOL:
            error(tree.meta.operand_2, "boolean type expected")

        tree.meta.type = TYPES.BOOL

    @beartype
    def expr_logical_and(self, tree: lark.Tree):
        self._logical_common(tree, OPERATORS.OR)

    @beartype
    def expr_logical_or(self, tree: lark.Tree):
        self._logical_common(tree, OPERATORS.OR)
