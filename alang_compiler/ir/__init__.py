from typing import Optional, Union, Dict

import lark
from beartype import beartype

from alang_compiler.scope import Scope
from alang_compiler.utils import get_metadata, OPERATORS, Operator, debug, error, warn, critical, info, Debug, TYPES, \
    Type
import inspect


class IRAtomic:
    def __init__(self, used_as_storage: Optional[list['Info']] = None, used_as_value: Optional[list['Info']] = None):
        if used_as_storage is not None:
            for var in used_as_storage:
                var.last_usage_as_storage = self

        if used_as_value is not None:
            for var in used_as_value:
                var.last_usage_as_value = self

        self.used_as_storage = used_as_storage
        self.used_as_value = used_as_value
        self.line = -1

    def __str__(self):
        return self.__class__.__name__

    def __repr__(self):
        return self.__str__()

    def get_line(self) -> int:
        return self.line

    def get_last_usages(self) -> str:
        if Debug.DEBUG_LEVEL <= 5:
            return ""

        as_value: list['Info'] = []
        as_storage: list['Info'] = []
        elements = []
        if self.used_as_storage is not None:
            elements += self.used_as_storage
        if self.used_as_value is not None:
            elements += self.used_as_value

        for el in elements:
            if isinstance(el, IRConst):
                continue
            if el.last_usage_as_value == self:
                as_value += [el]
            if el.last_usage_as_storage == self:
                as_storage += [el]

        msg = ""
        if len(as_storage):
            msg += "\t\t\t# Last usages as storage: " + ",".join(str(el.get_name()) for el in as_storage)
        if len(as_value):
            msg += "\t\t\t# Last usages as value: " + ",".join(str(el.get_name()) for el in as_value)
        return msg


class Info:
    def __init__(self):
        self.name: str = self.__class__.__name__
        self.last_usage_as_value: Optional[IRAtomic] = None
        self.last_usage_as_storage: Optional[IRAtomic] = None

    def __str__(self):
        return f"{self.__class__.__name__}({self.name})"

    def get_name(self) -> str:
        return self.name

    def get_last_usage_as_value_string(self) -> str:
        if self.last_usage_as_value is not None:
            return f"Last usage as value for {self.get_name()}: {str(self.last_usage_as_value.get_line())}"
        else:
            return ""

    def __repr__(self):
        return self.__str__()


class IRVariable(Info):
    temp_var_sequence: int = 0

    @beartype
    def __init__(self, name: Optional[str] = None):
        super().__init__()
        if name is not None:
            self.name = name
        else:
            self.name = f"t_{self.__class__.temp_var_sequence}"
            self.__class__.temp_var_sequence += 1

    def get_name(self):
        return self.name


class IRConst(IRVariable):
    @beartype
    def __init__(self, value: Union[int, bool]):
        super().__init__()
        self.value = int(value)

    def get_name(self):
        return self.value

    def __str__(self):
        return f"{self.__class__.__name__}({self.value})"


class IRLabel(Info):
    temp_label_sequence = 0

    @beartype
    def __init__(self, name: Optional[str] = None):
        super().__init__()
        self.line = -1
        if name is not None:
            self.name = name
        else:
            self.name = f"L_{self.__class__.temp_label_sequence}"
            self.__class__.temp_label_sequence += 1

    def __str__(self):
        return self.name


class QCall(IRAtomic):
    @beartype
    def __init__(self, result: IRVariable, func_name: Union[IRLabel, str], arg_num: int):
        used_as_value = []
        if type(func_name) == IRLabel:
            used_as_value.append(func_name)

        super().__init__([result], used_as_value)
        self.func_name = func_name
        self.arg_num = arg_num
        self.result = result

    def __str__(self):
        return f"\t{self.result.get_name()} := call {self.func_name}<{self.arg_num}>" + \
               self.get_last_usages()


class QHalt(IRAtomic):
    @beartype
    def __init__(self):
        pass

    def __str__(self):
        return f"\thalt"


class QBinaryOperator(IRAtomic):
    @beartype
    def __init__(self, result: IRVariable, operand_1: Optional[IRVariable], operand_2: Optional[IRVariable],
                 operator: Operator):
        super().__init__([result], [operand_1, operand_2])
        self.result = result
        self.operand_1 = operand_1
        self.operand_2 = operand_2
        self.operator = operator

    def __str__(self):
        return f"\t{self.result.get_name()} := {self.operand_1.get_name()} {self.operator} {self.operand_2.get_name()}" + \
               self.get_last_usages()


class QAssign(IRAtomic):
    @beartype
    def __init__(self, result: IRVariable, value: IRVariable):
        super().__init__([result], [value])
        self.result = result
        self.value = value

    def __str__(self):
        return f"\t{self.result.get_name()} := {self.value.get_name()}" + self.get_last_usages()


class QIncrement(IRAtomic):
    @beartype
    def __init__(self, result: IRVariable):
        super().__init__([result], [result])
        self.result = result

    def __str__(self):
        return f"\tincrement {self.result.get_name()}" + self.get_last_usages()


class QParam(IRAtomic):
    @beartype
    def __init__(self, ir_var: IRVariable):
        super().__init__(None, [ir_var])
        self.ir_var = ir_var

    def __str__(self):
        return f"\tparam {self.ir_var.get_name()}" + self.get_last_usages()


class QReturn(IRAtomic):
    @beartype
    def __init__(self, ir_var: Optional[IRVariable] = None):
        as_value = []
        if ir_var is not None:
            as_value += [ir_var]
        super().__init__(None, as_value)
        self.ir_var = ir_var

    def __str__(self):
        if self.ir_var is not None:
            return f"\treturn {self.ir_var.get_name()}" + self.get_last_usages()
        else:
            return f"\treturn"


class QLabel(IRAtomic):
    @beartype
    def __init__(self, ir_label: IRLabel):
        super(QLabel, self).__init__([ir_label])
        self.ir_label = ir_label

    def __str__(self):
        return f"{self.ir_label.get_name()}:"


class QCondition(IRAtomic):
    """
    If condition is NOT met, jump to label
    """

    @beartype
    def __init__(self, ir_var: IRVariable, false_label: IRLabel):
        super().__init__(None, [ir_var, false_label])
        self.ir_var = ir_var
        self.false_label = false_label

    def __str__(self):
        return f"\tiffalse {self.ir_var.get_name()}, {self.false_label.get_name()}" + \
               self.get_last_usages()


class QJump(IRAtomic):
    @beartype
    def __init__(self, label: IRLabel):
        super().__init__(None, [label])
        self.label = label

    def __str__(self):
        return f"\tjump {self.label.get_name()}" + self.get_last_usages()


class QAllocate(IRAtomic):
    @beartype
    def __init__(self, ir_var: IRVariable, size: int):
        super().__init__([ir_var], None)
        self.ir_var = ir_var
        self.size = size

    def __str__(self):
        return f"\tallocate {self.ir_var.get_name()}, {self.size}"


class IRComment(IRAtomic):
    @beartype
    def __init__(self, position: dict[str, int], value: str):
        super().__init__()
        self.position = position
        self.value = value.replace("//", "").strip().replace("\n", "\n# ")

    def __str__(self):
        return f"# Source comment ({self.position['line']}:{self.position['column']}): {self.value}"


def scoped_identifier(scope: list[str], name: str):
    return "_".join(scope + [name])


# noinspection PyMethodMayBeStatic
class Intermediate(lark.visitors.Interpreter):
    @beartype
    def __init__(self, root_scope: Scope):
        self.functions_args: dict[str, list[IRVariable]] = {}
        self.root_scope = root_scope
        self.IR: list[IRAtomic] = []

        self.IR_header: list[IRAtomic] = []

        self.IRVariables: dict[str, IRVariable] = {}
        self.IRLabels: dict[str, IRLabel] = {}
        self.IRConsts: list[IRConst] = []
        self.types: dict[str, Type] = {}
        self.in_function = False

    def __default__(self, tree: lark.Tree):
        info(tree, f"{tree.data} does not have a custom visitor", importance=5)
        self.visit_children(tree)
        return tree

    def visit_children(self, tree: lark.Tree) -> list[lark.Tree]:
        try:
            res = super().visit_children(tree)
            debug(tree, "Visiting children of " + tree.data, importance=6)
            return res
        except Exception as e:
            critical(tree, "Compiler error:", e)
            if Debug.DEBUG_LEVEL >= 4:
                raise e

    def visit(self, tree: lark.Tree) -> lark.Tree:
        try:
            res = super().visit(tree)
            debug(tree, f"Visiting: {tree.data}", importance=6)
            return res
        except Exception as e:
            critical(tree, "Compiler error:", e)
            if Debug.DEBUG_LEVEL >= 4:
                raise e

    def _new_var(self, name: Optional[str] = None, scope: Optional[list[str]] = None) -> IRVariable:

        if name is None:
            var = IRVariable()
            self.IRVariables[var.name] = var
            return var

        var_name = name
        var_scope = ["external"]

        scope = self.root_scope.lookup_scope(scope)
        var_definition = scope.lookup_variable(var_name)
        if var_definition is not None:
            var_scope = var_definition.meta.scope

        var_identifier = scoped_identifier(var_scope, var_name)
        if var_identifier in self.IRVariables:
            error(None, f"issue in IR, IRVariable {var_identifier} defined twice. This is a compiler bug.")
            return self.IRVariables[var_identifier]
        else:
            var = IRVariable(var_identifier)
            self.IRVariables[var_identifier] = var
            return var

    def _get_var(self, name: Optional[str] = None, scope: Optional[list[str]] = None):
        var_name = name
        var_scope = ["external"]

        scope = self.root_scope.lookup_scope(scope)
        var_definition = scope.lookup_variable(var_name)
        if var_definition is not None:
            var_scope = var_definition.meta.scope

        var_identifier = scoped_identifier(var_scope, var_name)
        if var_identifier in self.IRVariables:
            return self.IRVariables[var_identifier]
        else:
            error(None,
                  f"issue in IR, IRVariable {var_identifier} is not defined. This is a compiler bug\n",
                  f"Defined variables: {self.IRVariables}"
                  )

    def _get_type_size(self, type: Union[Type, str]) -> int:
        if isinstance(type, Type):
            return type.size
        elif type in self.types:
            return self.types[type]
        else:
            raise IndexError(f"Type '{type}' is unknown!")

    def _add_label(self, label: Optional[str] = None) -> IRLabel:
        ir_label = IRLabel(label)
        self.IRLabels[ir_label.get_name()] = ir_label
        return ir_label

    def function_call(self, tree: lark.Tree):
        self.visit_children(tree)

        func_name = tree.meta.name
        func_args: list[lark.Tree] = tree.meta.arguments
        func_scope = tree.meta.namespace

        scope = self.root_scope.lookup_scope(tree.meta.scope)
        func_definition = scope.lookup_function(func_name)
        if func_definition is not None:
            func_scope = func_definition.meta.scope

        full_function_name = scoped_identifier(func_scope, func_name)
        function_label = self.IRLabels[full_function_name]

        result = self._new_var()
        self.IR += [QAllocate(result, self._get_type_size(tree.meta.type))]
        for arg in func_args:
            try:
                self.IR += [QParam(arg.meta.ir_var)]
            except AttributeError as e:
                error(arg, e, get_metadata(arg.meta), arg.data)
        self.IR += [QCall(result, function_label, len(func_args))]

        tree.meta.ir_var = result

        return tree

    @beartype
    def expression(self, tree: lark.Tree):
        self.visit_children(tree)

        debug(tree, "Expression:", len(tree.children), get_metadata(tree.children[0].meta), tree.children[0].data)
        if len(tree.children) == 1:
            tree.meta.ir_var = tree.children[0].meta.ir_var
        else:
            raise Exception('Wat da fuck')

        return tree

    def expr_assignment(self, tree: lark.Tree):
        self.visit_children(tree)
        result = self._get_var(tree.meta.name, tree.meta.scope)
        value = tree.children[1].meta.ir_var
        tree.meta.ir_var = result
        self.IR += [QAssign(result, value)]

        return tree

    def expr_increment(self, tree: lark.Tree):
        self.visit_children(tree)
        result = self._get_var(tree.meta.name, tree.meta.scope)
        tree.meta.ir_var = result
        self.IR += [QIncrement(result)]

    @beartype
    def axiom(self, tree: lark.Tree):
        entrypoint_label = self._add_label("entrypoint")
        exit_status = IRVariable("exit_status")
        self.IRVariables["exit_status"] = exit_status

        self.IR_header += [QLabel(entrypoint_label)]
        self.IR_header += [QAllocate(exit_status, self._get_type_size(TYPES.INT))]

        self.visit_children(tree)

        self.IR_header += [QHalt()]

        self.IR = self.IR_header + self.IR

        return tree

    def namespace_declaration(self, tree: lark.Tree):
        self.visit_children(tree)

    def identifier(self, tree: lark.Tree):
        self.visit_children(tree)

    def decorated_function(self, tree: lark.Tree):
        self.visit_children(tree)

        debug(tree, "Called decorated_function")
        if tree.meta.decorator == "entrypoint":
            debug(tree, "Found entrypoint decorator")
            debug(tree, get_metadata(tree.meta))
            function_identifier = scoped_identifier(tree.meta.decorated_function.meta.scope,
                                                    tree.meta.decorated_function.meta.name)

            function_label = self.IRLabels[function_identifier]

            if tree.meta.arg_number == 2:
                argc = IRVariable("argc")
                argv = IRVariable("argv")
                self.IRVariables["argc"] = argc
                self.IRVariables["argv"] = argv
                self.IR_header += [QParam(argc), QParam(argv)]

            self.IR_header += [QCall(self.IRVariables["exit_status"], function_label, tree.meta.arg_number)]

    def return_statement(self, tree: lark.Tree):
        self.visit_children(tree)
        if len(tree.children) == 1:
            value = tree.children[0].meta.ir_var
            self.IR += [QReturn(value)]
        else:
            self.IR += [QReturn()]

        return tree

    def break_statement(self, tree: lark.Tree):
        self.visit_children(tree)
        false_label = tree.meta.loop.meta.false_label
        self.IR += [QJump(false_label)]

    def variable_declaration(self, tree: lark.Tree):
        self.visit_children(tree)

        temp_IR = []

        result = self._new_var(tree.meta.name, tree.meta.scope)
        temp_IR += [QAllocate(result, self._get_type_size(tree.meta.type))]

        if tree.meta.expression is not None:
            value = tree.meta.expression.meta.ir_var
            tree.meta.ir_var = result
            temp_IR += [QAssign(result, value)]

        if self.in_function:
            self.IR += temp_IR
        else:
            self.IR_header += temp_IR

        return tree

    def expr_identifier(self, tree: lark.Tree):
        self.visit_children(tree)

        tree.meta.ir_var = self._get_var(tree.meta.name, tree.meta.scope)
        return tree

    def while_loop(self, tree: lark.Tree):
        false_label, loop_start = self._loop_common(tree)
        self.IR += [QJump(loop_start)]
        self.IR += [QLabel(false_label)]

    def for_loop(self, tree: lark.Tree):

        # We init the loop first
        self.visit(tree.meta.initialization)

        false_label, loop_start = self._loop_common(tree)

        self.visit(tree.meta.incrementation)

        self.IR += [QJump(loop_start)]
        self.IR += [QLabel(false_label)]

    def _loop_common(self, tree: lark.Tree):
        loop_start = self._add_label()
        false_label = self._add_label()
        tree.meta.false_label = false_label
        self.IR += [QLabel(loop_start)]
        self.visit(tree.meta.condition)
        condition_var = tree.meta.condition.meta.ir_var
        self.IR += [QCondition(condition_var, false_label)]
        self.visit(tree.meta.statement)
        return false_label, loop_start

    def if_statement(self, tree: lark.Tree):
        true_label = None
        self.visit(tree.meta.condition)
        condition_var = tree.meta.condition.meta.ir_var  # self._new_var()
        false_label = self._add_label()
        self.IR += [QCondition(condition_var, false_label)]

        self.visit(tree.meta.iftrue)

        if tree.meta.iffalse is not None:
            true_label = self._add_label()
            self.IR += [QJump(true_label)]

        self.IR += [QLabel(false_label)]

        if tree.meta.iffalse is not None:
            self.visit(tree.meta.iffalse)
            self.IR += [QLabel(true_label)]

    def _generic_expr_binary_op(self, tree: lark.Tree):
        self.visit_children(tree)

        result = self._new_var()
        self.IR += [QAllocate(result, self._get_type_size(tree.meta.type))]
        operand_1 = None
        try:
            operand_1 = tree.meta.operand_1.meta.ir_var
        except AttributeError as e:
            error(tree.meta.operand_1, e, get_metadata(tree.meta.operand_1))

        operand_2 = tree.meta.operand_2.meta.ir_var

        operator = tree.meta.operator
        self.IR += [QBinaryOperator(result, operand_1, operand_2, operator)]

        tree.meta.ir_var = result
        return tree

    def expr_sum_sub(self, tree: lark.Tree):
        return self._generic_expr_binary_op(tree)

    def expr_mul_div_mod(self, tree: lark.Tree):
        return self._generic_expr_binary_op(tree)

    def expr_equals_nequals(self, tree: lark.Tree):
        return self._generic_expr_binary_op(tree)

    def expr_lesser_greater(self, tree: lark.Tree):
        return self._generic_expr_binary_op(tree)

    def expr_logical_and(self, tree: lark.Tree):
        return self._generic_expr_binary_op(tree)

    def expr_logical_or(self, tree: lark.Tree):
        return self._generic_expr_binary_op(tree)

    def literal(self, tree: lark.Tree):
        self.visit_children(tree)
        debug(tree, "literal", tree.children, get_metadata(tree.meta))
        tree.meta.ir_var = IRConst(tree.meta.value)
        self.IRConsts += [tree.meta.ir_var]
        return tree

    def comment(self, tree: lark.Tree):
        self.visit_children(tree)
        self.IR += [IRComment({"line": tree.line, "column": tree.column}, tree.children[0])]

        return tree

    def function(self, tree: lark.Tree):
        func_name = tree.meta.name
        func_scope = tree.meta.scope

        new_label = self._add_label(scoped_identifier(func_scope, func_name))

        self.IR += [QLabel(new_label)]

        self.functions_args[new_label.get_name()] = []
        for i, formal in enumerate(tree.meta.formals):
            formal_ir = self._new_var(formal.meta.name, formal.meta.scope)
            self.functions_args[new_label.get_name()] += [formal_ir]
            self.IR += [QAllocate(formal_ir, self._get_type_size(formal.meta.type))]

        previous_state, self.in_function = self.in_function, True
        self.visit_children(tree)
        self.in_function = previous_state

        return tree

    def _update_ir_lines(self):
        for line, ir in enumerate(self.IR):
            if isinstance(ir, QLabel):
                ir.ir_label.line = line + 1
            ir.line = line + 1

    def _get_ir_text(self) -> str:
        self._update_ir_lines()

        s = "# Alang IR\n"
        s += "\n".join(f"{x.get_line()}:\t{str(x)}" for x in self.IR)

        s += "\n== Metadata ==\n"

        s += "IR Labels: " + str([label.get_name() for i, label in self.IRLabels.items()])
        s += "\n"
        s += "IR Variables: " + str([var.get_name() for var in list(self.IRVariables.values())])
        s += "\n"
        s += "IR Consts: " + str([const.get_name() for const in self.IRConsts])
        return s

    def __str__(self):
        s = "\n======= Intermediate =====\n"
        s += self._get_ir_text()
        if Debug.DEBUG_LEVEL >= 4:
            s += "\n== Last usages information ==\n"
            s += "\n".join(value.get_last_usage_as_value_string() for key, value in self.IRVariables.items())
        return s

    def __repr__(self):
        return self.__str__()


# noinspection PyMethodMayBeStatic
class IRInterpreter:
    def __init__(self, intermediate: Intermediate, no_default: bool = False):
        self.intermediate: Intermediate = intermediate
        self.no_default: bool = no_default
        self.cursor: int = 0
        self.halted: bool = False

    def __default__(self, arg):
        if self.no_default:
            raise NotImplementedError(
                f'{self.__class__.__name__} was specified to not ignore any unimplemented IRAtomic operation, ' +
                f'and {arg.__class__.__name__} was found')

    def evaluate(self):
        debug(None, f"Evaluating {self.__class__.__name__}")
        while not self.halted:
            ir = self.intermediate.IR[self.cursor]
            debug(None, f"cursor={self.cursor}: {str(ir)}", importance=3)
            if not isinstance(ir, IRAtomic):
                raise TypeError(f'IR should only contain IRAtomic objects, but a {ir.__class__.__name__} was found')
            getattr(self, ir.__class__.__name__, self.__default__)(ir)
            self.cursor += 1
            if self.cursor >= len(self.intermediate.IR):
                self.halted = True
