from typing import Optional

import lark
from . import warn


def is_constexpr(tree: lark.Tree) -> bool:
    if not( hasattr(tree, "data") or hasattr(tree, "meta") ):
        raise AttributeError(f"'{tree}' of type '{type(tree)}' has no attribute 'data' or 'meta'")

    if tree.data == 'literal':
        return True

    if hasattr(tree.meta, 'constexpr') and tree.meta.constexpr:
        return True

    return all(
        hasattr(child.meta, 'constexpr') and child.meta.constexpr
        for child in tree.children
    )


def replace_tree(old_tree: lark.Tree, new_tree: lark.Tree):
    old_tree.__dict__ = new_tree.__dict__


def overflow_emulation(value: int, byte_number: int, tree: Optional[lark.Tree] = None) -> int:
    overflow_value = 2 ** (8 * byte_number)
    if value >= overflow_value:
        warn(tree, "overflow in constant evaluation")
    return value % overflow_value
