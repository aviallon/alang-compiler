from beartype import beartype
import lark

from . import Scope
from .utilities import is_constexpr, overflow_emulation, replace_tree
from ..utils import get_metadata, debug, OPERATORS, TYPES, get_value, quote_tree, warn, error


class OptimizeConstExpressions(lark.visitors.Interpreter):
    def __init__(self, root_scope: Scope):
        super().__init__()
        self.root_scope = root_scope

    @beartype
    def __default__(self, tree: lark.Tree):
        self.visit_children(tree)
        return tree

    @beartype
    def visit(self, tree: lark.Tree):
        super().visit(tree)
        if hasattr(tree.meta, 'constexpr'):
            debug(tree, tree.data, "Metadata: ", get_metadata(tree.meta), importance=6)
        return tree

    @beartype
    def _generic_expr_binary_op(self, tree: lark.Tree):
        self.visit_children(tree)
        tree.meta.constexpr = is_constexpr(tree)
        new_tree = None
        new_value = None

        if tree.meta.constexpr:
            operand_1 = tree.meta.operand_1.meta.value
            operand_2 = tree.meta.operand_2.meta.value
            debug(tree, "can be simplified, operator:", tree.meta.operator)
            if tree.meta.operator == OPERATORS.PLUS:
                new_value = operand_1 + operand_2
            elif tree.meta.operator == OPERATORS.MINUS:
                new_value = operand_1 - operand_2
            elif tree.meta.operator == OPERATORS.DIVIDE:
                new_value = operand_1 // operand_2
            elif tree.meta.operator == OPERATORS.TIMES:
                new_value = operand_1 * operand_2
            elif tree.meta.operator == OPERATORS.MODULO:
                new_value = operand_1 % operand_2
            elif tree.meta.operator == OPERATORS.AND:
                new_value = operand_1 and operand_2
            elif tree.meta.operator == OPERATORS.OR:
                new_value = operand_1 or operand_2
            elif tree.meta.operator == OPERATORS.EQUALS:
                new_value = operand_1 == operand_2
            elif tree.meta.operator == OPERATORS.DIFFERENT:
                new_value = operand_1 != operand_2
            elif tree.meta.operator == OPERATORS.LESS_EQUAL:
                new_value = operand_1 <= operand_2
            elif tree.meta.operator == OPERATORS.LESS:
                new_value = operand_1 < operand_2
            elif tree.meta.operator == OPERATORS.GREATER_EQUAL:
                new_value = operand_1 >= operand_2
            elif tree.meta.operator == OPERATORS.GREATER:
                new_value = operand_1 > operand_2
            else:
                error(tree, "Unknown operator for optimizer:", tree.meta.operator)

        else:
            if tree.meta.operator in [OPERATORS.AND, OPERATORS.OR]:
                const_operand, other_operand = None, None
                if is_constexpr(tree.meta.operand_1):
                    const_operand = tree.meta.operand_1
                    other_operand = tree.meta.operand_2
                elif is_constexpr(tree.meta.operand_2):
                    const_operand = tree.meta.operand_2
                    other_operand = tree.meta.operand_1

                if const_operand is not None:
                    debug(const_operand, "const_operand", get_metadata(const_operand.meta), const_operand)
                    if const_operand.meta.value == (
                        tree.meta.operator != OPERATORS.AND
                    ):
                        new_value = const_operand.meta.value
                    else:
                        new_tree = other_operand

        if new_value is not None:
            new_value = overflow_emulation(new_value, TYPES.INT.size, tree)

            new_tree = lark.Tree("literal", [new_value], tree.meta)
            new_tree.meta.value = new_value

        if new_tree is not None:
            replace_tree(tree, new_tree)

    @beartype
    def literal(self, tree: lark.Tree):
        self.visit_children(tree)
        tree.meta.constexpr = True

    @beartype
    def expression(self, tree: lark.Tree):
        self.visit_children(tree)
        tree.meta.constexpr = is_constexpr(tree)

    @beartype
    def expr_sum_sub(self, tree: lark.Tree):
        self._generic_expr_binary_op(tree)

    @beartype
    def expr_mul_div_mod(self, tree: lark.Tree):
        self._generic_expr_binary_op(tree)

    @beartype
    def expr_lesser_greater(self, tree: lark.Tree):
        self._generic_expr_binary_op(tree)

    @beartype
    def expr_logical_or(self, tree: lark.Tree):
        self._generic_expr_binary_op(tree)

    @beartype
    def expr_logical_and(self, tree: lark.Tree):
        self._generic_expr_binary_op(tree)

    @beartype
    def expr_equals(self, tree: lark.Tree):
        self._generic_expr_binary_op(tree)

    @beartype
    def while_loop(self, tree: lark.Tree):
        self._loop_common(tree)

    @beartype
    def for_loop(self, tree: lark.Tree):
        self._loop_common(tree)

    def _loop_common(self, tree):
        self.visit_children(tree)
        constexpr = is_constexpr(tree.meta.condition)
        if constexpr:
            val = get_value(tree, tree.meta.condition)
            if not val:
                msg = "Condition is always false!\n"
                msg += quote_tree(tree.meta.condition)
                warn(tree, msg)
                new_tree = lark.Tree("comment", [f"Optimized out: {tree.data}"], tree.meta)
                replace_tree(tree, new_tree)
            else:
                warn(tree, "Infinite-loop!")

    @beartype
    def if_statement(self, tree: lark.Tree):
        self.visit_children(tree)
        constexpr = is_constexpr(tree.meta.condition)
        if constexpr:
            val = get_value(tree, tree.meta.condition)
            if not val:
                msg = "Condition is always false!\n"
                msg += quote_tree(tree.meta.condition)
                warn(tree, msg)
                new_tree = tree.meta.iffalse
            else:
                msg = "Condition is always true!\n"
                msg += quote_tree(tree.meta.condition)
                warn(tree, msg)
                new_tree = tree.meta.iftrue
            replace_tree(tree, new_tree)
