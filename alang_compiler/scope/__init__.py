from typing import Dict, Optional

import lark
from beartype import beartype

from alang_compiler.utils import debug


class Scope:
    anonymous_scope_sequence = 0

    @beartype
    def __init__(self, name: Optional[str], parent: Optional['Scope'] = None, is_namespace: bool = False):
        self.name = name
        if name is None:
            self.name = str(Scope.anonymous_scope_sequence)
            Scope.anonymous_scope_sequence += 1

        self.parent: Optional[Scope] = parent
        self.scopes: list[Scope] = []
        debug(None, f"New scope '{self.name}' with parent '{self.parent}'")
        self.functions: Dict[str, lark.Tree] = {}
        self.variables: Dict[str, lark.Tree] = {}
        self.entrypoint: Optional[lark.Tree] = None
        self.is_namespace = is_namespace
        self.last_reachable_statement: Optional[lark.Tree] = None

    @beartype
    def get_scope(self) -> list[str]:
        if self.parent is None:
            return [self.name]
        else:
            return self.parent.get_scope() + [self.name]

    @beartype
    def add_scope(self, scope: 'Scope'):
        scope.parent = self
        self.scopes.append(scope)
        return scope

    @beartype
    def lookup_variable(self, name: str) -> Optional[lark.Tree]:
        if name in self.variables:
            return self.variables[name]
        elif self.parent is not None:
            if (var := self.parent.lookup_variable(name)) is not None:
                return var

        return None

    @beartype
    def lookup_function(self, name: str) -> Optional[lark.Tree]:
        if name in self.functions:
            return self.functions[name]
        elif self.parent is not None:
            if (func := self.parent.lookup_function(name)) is not None:
                return func

        return None

    @beartype
    def lookup_scope(self, scope_list: list[str]):
        if not len(scope_list):
            raise Exception('scope_list is empty!')

        if scope_list[0] != self.name:
            return None

        if len(scope_list) == 1:
            return self

        for scope in self.scopes:
            temp = scope.lookup_scope(scope_list[1:])
            if temp is not None:
                return temp

        raise Exception(f'Scope not found: {scope_list}')

    def print_all_sub_scopes(self, depth: int = 0) -> None:
        if self.parent is None:
            print("Scopes: ")
        connection_char = ""
        if self.parent is not None:
            connection_char = "└─"
            if len(self.scopes):
                connection_char = "└─"
        print("    " * depth + connection_char + " " + self.name)
        for scope in self.scopes:
            scope.print_all_sub_scopes(depth + 1)

    def print_all(self, depth: int = 0):
        indentation = "  "
        print(indentation * depth, "Scope: ", self.name)
        for key, value in self.variables.items():
            print(indentation * (depth + 1), f"Variable: {value.meta.type} {value.meta.name}")

        for key, value in self.functions.items():
            function_formals = [f"{formal.meta.type} {formal.meta.name}" for formal in value.meta.formals]
            print(indentation * (depth + 1),
                  f"Function: {value.meta.type} {value.meta.name} ({','.join(function_formals)})")

        for scope in self.scopes:
            scope.print_all(depth + 1)

    def __repr__(self) -> str:
        return " -> ".join(self.get_scope())

