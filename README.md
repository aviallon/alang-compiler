# Alang Compiler
A compiler for a C-like language called A, written in Python with the Lark library.
Work in progress, currently able to generate IR, and still has some bugs/ambiguity issues.

# Progress
- [ ] Grammar
	- [X] Base
	- [X] Statements
	- [X] Function declarations
	- [X] Variable declarations
	- [X] Function calls
	- [ ] Arithmetic
		- [X] Sum & Subtraction
		- [ ] Modulo
		- [ ] Mul & Div
		- [ ] Power
	- [X] Global variables
- [X] Lexer & Paser (automagically done by Lark)
- [X] Tree annotation (semantic tree)
- [X] Intermediate Represenation generation
- [ ] All known bugs fixed

# Why it's on Gitlab
I had a terrible time trying to figure out how it worked on my own. Here is my result, after the **EIGHTH** failed attempt.
The hardest part was trying to find a well-document library, with good examples. Unfortunately, Lark examples were a bit too simple or too specific, so it wasn't appropriate...  
So here it is: I hope my project is organized enough to be understandable by other people. If not, feel free to create an issue.
