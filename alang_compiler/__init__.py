from typing import Dict, Optional
import sys
import lark
import concurrent.futures

from alang_compiler.interpreter import EvaluateIR
from alang_compiler.ir import Intermediate
from alang_compiler.optimizer import OptimizeConstExpressions, DeadCodeRemover
from alang_compiler.optimizer.ir_optimizer import IROptimizer
from alang_compiler.scope import Scope
from alang_compiler.semantic import TreeReducer, AnnotateTree, AmbiguityRemover
from alang_compiler.utils import Debug, info, critical

thread_pool = concurrent.futures.ThreadPoolExecutor()


def async_pydot_to_png(tree: lark.Tree, filename: str):
    tree_copy = lark.Transformer().transform(tree)
    thread = thread_pool.submit(lark.tree.pydot__tree_to_png, tree_copy,
                       filename)
    thread_pool.submit(lambda t: info(tree_copy, f"Finished Dot generation for {filename} (output: {t.result()})"), thread)


def compile_input(input_data: str, with_ambiguity: bool = False, flags: Optional[Dict[str, bool]] = None):
    if flags is None:
        flags = {}

    Debug.input_lines = input_data.split("\n")
    Debug.DEBUG_LEVEL = flags["debug_level"] if "debug_level" in flags else 3

    info(None, "Flags: ", flags)

    config = dict(cache=True,
                  start='axiom',
                  parser='lalr',
                  lexer='contextual',
                  propagate_positions=True)

    if with_ambiguity:
        config = dict(parser='earley',
                      start='axiom',
                      lexer='dynamic',
                      propagate_positions=True,
                      ambiguity='explicit')

    info(None, "Grammar compilation phase")
    # noinspection Mypy
    parser = lark.Lark(
        open("alang_compiler/spec/alang.lark", "r").read(),
        **config
    )

    info(None, "Parsing phase")

    parse_tree = parser.parse(input_data)
    reduced_parse_tree = TreeReducer().transform(parse_tree)
    async_pydot_to_png(reduced_parse_tree, "ambiguous_tree.png")

    if with_ambiguity:
        reduced_parse_tree = AmbiguityRemover().transform(reduced_parse_tree)
    async_pydot_to_png(reduced_parse_tree, "tree.png")

    info(None, "Annotation phase")
    root_scope = Scope('root')

    annotate_tree = AnnotateTree(root_scope)
    semantic_tree = annotate_tree.visit(reduced_parse_tree)

    if root_scope.entrypoint is None:
        critical(semantic_tree, "No entrypoint found! Please decorate a function with @entrypoint.")
        exit(1)

    root_scope.print_all()

    optimized_tree = semantic_tree

    info(None, "Optimization phase")
    if "const_eval" in flags and flags["const_eval"]:
        const_eval_transformer = OptimizeConstExpressions(root_scope)
        optimized_tree = const_eval_transformer.visit(optimized_tree)

    if "dead_code_remover" in flags and flags["dead_code_remover"]:
        info(None, "Dead code remover not ready yet")
        # dead_code_remover = DeadCodeRemover(root_scope)
        # optimized_tree = dead_code_remover.visit(optimized_tree)

    async_pydot_to_png(optimized_tree, "optimized_tree.png")

    if 'error' in Debug.message_counts:
        print(f"{Debug.message_counts['error']} errors occured during semantic analysis, aborting")
        sys.exit(127)

    info(None, "IR generation phase")
    intermediate = Intermediate(root_scope)

    intermediate.visit(optimized_tree)

    print(intermediate)

    # ir_optimizer = IROptimizer(intermediate)
    # ir_optimizer.evaluate()

    evaluate_compiled_code = True
    if evaluate_compiled_code:
        evaluate_ir = EvaluateIR(intermediate)
        evaluate_ir.evaluate()

    if 'error' in Debug.message_counts:
        print(f"{Debug.message_counts['error']} errors encountered during compilation")
        sys.exit(127)
