from ..ir import *


# noinspection PyMethodMayBeStatic
class IROptimizer(IRInterpreter):
    def __init__(self, intermediate: Intermediate):
        super().__init__(intermediate)