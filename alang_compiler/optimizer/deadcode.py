import lark

from . import Scope
from . import warn, debug


def is_pure(tree: lark.Tree):
    if not hasattr(tree, "meta"):
        return True

    if hasattr(tree.meta, "pure"):
        return tree.meta.pure

    if len(tree.children):
        return all(is_pure(child) for child in tree.children)

    return False


class DeadCodeRemover(lark.visitors.Interpreter):
    def __init__(self, root_scope: Scope):
        self.root_scope = root_scope

    def __default__(self, tree: lark.Tree):
        self.visit_children(tree)
        tree.meta.pure = is_pure(tree)

    def visit(self, tree: lark.Tree):
        super().visit(tree)
        if hasattr(tree, "data"):
            debug(tree, f"{tree.data} pure: {is_pure(tree)}")

    def literal(self, tree: lark.Tree):
        self.visit_children(tree)
        tree.meta.pure = True

    def expression(self, tree: lark.Tree):
        self.visit_children(tree)
        tree.meta.pure = is_pure(tree)

    def integer(self, tree: lark.Tree):
        self.visit_children(tree)
        tree.meta.pure = True
