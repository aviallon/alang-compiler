from typing import Optional

import lark
from beartype import beartype
from copy import deepcopy

from alang_compiler import utils
from alang_compiler.scope import Scope
from alang_compiler.utils import debug, get_metadata, OPERATORS, TYPES, get_value, warn, quote_text, quote_tree
from .deadcode import DeadCodeRemover
from .constexpr import OptimizeConstExpressions


