import logging
import argparse

from alang_compiler import compile_input
from alang_compiler.utils import info

logging.basicConfig(level=logging.NOTSET)

if __name__ == '__main__':
    debug_level = 2

    parser = argparse.ArgumentParser()
    parser.add_argument(metavar="INPUT", dest="input", type=str)
    parser.add_argument("-fno-const-eval", dest="no_const_eval", action="store_true", default=False,
                        help="Do not optimize expression and statements which can be easily evaluated at compile time.")
    parser.add_argument("-Wunused-result", dest="warn_unused_result", action="store_true", default=False,
                        help="Warn if the return value of a function or expression is discarded.")
    parser.add_argument("-d", "--debug", action="store_const", const=True, default=False, required=False,
                        help="Print a lot of debug information, mainly useful for compiler development.")
    parser.add_argument('--verbose', '-v', action='count', default=0,
                        help="Progressively increase verbosity.")
    args = parser.parse_args()

    if args.debug:
        debug_level = 6

    if args.verbose != 0:
        debug_level += args.verbose

    with open(args.input, "r") as f:
        input_data = f.read()

    flags = {
        "const_eval": not args.no_const_eval,
        "dead_code_remover": True,
        "warn_unused_result": args.warn_unused_result,
        "debug_level": debug_level,
    }

    compile_input(input_data, with_ambiguity=True, flags=flags)
